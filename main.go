package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/Masterminds/semver/v3"
)

const defaultVersion = "0.1.0"

func main() {
	// Get working directory
	wd, e := os.Getwd()
	if e != nil {
		panic("failed to get working directory")
	}

	// Version file path
	path := wd + "/version"

	// Create version file if not exits
	e = createIfNeeded(path)
	if e != nil {
		panic("failed to create version file")
	}

	// Read version string from file
	vStr, e := read(path)
	if e != nil {
		panic("failed to read version file")
	}

	// Parse version
	v, e := semver.NewVersion(vStr)
	if e != nil {
		panic("failed to parse version string")
	}

	// Modify version based on level
	if len(os.Args[1:]) > 0 {
		level := os.Args[1]
		switch level {
		case "major", "m":
			v = bumpMajor(v)
		case "minor", "n":
			v = bumpMinor(v)
		case "patch", "p":
			v = bumpPatch(v)
		}

		// Write new version
		e = write(path, v.String())
		if e != nil {
			panic("failed to write version file")
		}
	}

	fmt.Println("v" + v.String())
}

func bumpMajor(v *semver.Version) *semver.Version {
	x := v.IncMajor()
	return &x
}
func bumpMinor(v *semver.Version) *semver.Version {
	x := v.IncMinor()
	return &x
}
func bumpPatch(v *semver.Version) *semver.Version {
	x := v.IncPatch()
	return &x
}

func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

func createIfNeeded(path string) error {
	if !exists(path) {
		return write(path, defaultVersion)
	}
	return nil
}

func read(path string) (string, error) {
	content, e := ioutil.ReadFile(path)
	return string(content), e
}

func write(path, content string) error {
	return ioutil.WriteFile(path, []byte(content), 0644)
}
