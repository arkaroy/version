# Version

Update `version` file in current directory with semantic versioning.

### To update major version
```
version m
```

### To update minor version
```
version n
```

### To update patch version
```
version p
```